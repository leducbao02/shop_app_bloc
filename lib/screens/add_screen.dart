import 'package:bloc_http_flutter/Utils/utils.dart';
import 'package:bloc_http_flutter/bloc/bloc_food/bloc_food_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/bloc_food/bloc_food_event.dart';
import '../widgets/text_input.dart';

class AddScreen extends StatefulWidget {
  const AddScreen({Key? key}) : super(key: key);

  @override
  State<AddScreen> createState() => _AddScreenState();
}

class _AddScreenState extends State<AddScreen> {
  late TextEditingController nameTextController;
  late TextEditingController priceTextController;
  late TextEditingController imageTextController;
  late TextEditingController descriptionTextController;

  @override
  void initState() {
    nameTextController = TextEditingController();
    priceTextController = TextEditingController();
    imageTextController = TextEditingController();
    descriptionTextController = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 90,
              color: Colors.blueAccent,
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: GestureDetector(onTap: () {
                        Navigator.of(context).pop();
                      },
                          child: Icon(
                            Icons.arrow_back_ios, color: Colors.white,))),
                  Container(margin: const EdgeInsets.fromLTRB(130, 0, 0, 0),
                      child: const Text('ADD', style: TextStyle(
                          color: Colors.white,
                          fontSize: 19,
                          fontWeight: FontWeight.bold),)),
                ],
              ),
            ),
            Column(
              children: [
                TextInput(textName: "Name",
                    hintText: "Enter name",
                    textEditingController: nameTextController),
                TextInput(textName: "Price",
                    hintText: "Enter price",
                    textEditingController: priceTextController),
                TextInput(textName: "Image",
                    hintText: "Enter image link",
                    textEditingController: imageTextController),
                TextInput(textName: "Description",
                    hintText: "Description",
                    textEditingController: descriptionTextController)
              ],
            ),
            const SizedBox(height: 200,),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: InkWell(
                onTap: () {
                  add();
                },
                child: Container(
                  height: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.blue,
                  ),
                  child: const Center(
                      child: Text('SAVE', style: TextStyle(color: Colors
                          .white, fontWeight: FontWeight.bold),)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  add() {
    String name = nameTextController.text.trim();
    String price = priceTextController.text.trim().toString();
    String image = imageTextController.text.trim();
    String des = descriptionTextController.text.trim();
    Utils.validate(name, price, image, des, context);
    if(name.isNotEmpty && price.isNotEmpty && image.isNotEmpty && des.isNotEmpty){
      BlocProvider.of<FoodBloc>(context).add(AddFood(name, price, image, des,context));

    }
  }
}
