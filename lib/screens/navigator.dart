import 'package:bloc_http_flutter/screens/oder_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/bloc_food/bloc_food_bloc.dart';
import '../bloc/bloc_food/bloc_food_event.dart';
import 'home_screen.dart';

class ScreenNav extends StatefulWidget {
  const ScreenNav({Key? key}) : super(key: key);

  @override
  State<ScreenNav> createState() => _ScreenNavState();
}

class _ScreenNavState extends State<ScreenNav> {
  int _currenIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<FoodBloc>(context).add(FetchLisFood());
  }

  List tab = [const HomeScreen(), const OderScreen()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: tab[_currenIndex],
        bottomNavigationBar: BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
            BottomNavigationBarItem(
              icon: Icon(Icons.border_all_rounded),
              label: "Order",
            ),
          ],
          currentIndex: _currenIndex,
          onTap: (value) {
            setState(() {
              _currenIndex = value;
            });
          },
        ));
  }
}
