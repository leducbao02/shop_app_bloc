import 'package:bloc_http_flutter/bloc/bloc_food/bloc_food_bloc.dart';
import 'package:bloc_http_flutter/bloc/bloc_order/order_bloc.dart';
import 'package:bloc_http_flutter/models/Food_model.dart';
import 'package:bloc_http_flutter/screens/add_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/bloc_food/bloc_food_state.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // final FoodBloc _foodBloc = FoodBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _foodBloc.add(Fet  chLisFood());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            color: Colors.blueAccent,
            height: 100,
            child: const Center(
                child: Text(
              'HOME',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            )),
          ),
          BlocBuilder<FoodBloc, FoodState>(
            builder: (context, state) {
              if (state is FoodLoading) {
                return const Center(child: CircularProgressIndicator());
              }
              if (state is FoodLoaded) {
                print(state.listFood.length);
                return Expanded(
                  child: ListView.builder(
                      itemCount: state.listFood.length,
                      // shrinkWrap: true,
                      itemBuilder: (context, index) =>
                          _itemHome(state.listFood, index, context)),
                );
              }
              if (state is FoodLoadError) {
                return Text(state.messge);
              }
              return Container();
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (bulider) => const AddScreen(),
            ),
          );
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}

Widget _itemHome(List<FoodModel> foodModel, int Index, BuildContext context) {
  return Card(
    child: ListTile(
      title: Wrap(
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child: SizedBox(
                  height: 90,
                  width: 63,
                  child: Image.network(foodModel[Index].image.toString()),
                ),
              ),
              Column(
                children: [
                  SizedBox(
                    width: 200,
                    child: Text(
                      overflow: TextOverflow.ellipsis,
                      foodModel[Index].name.toString(),
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.fromLTRB(0, 10, 140, 0),
                      child: Text(
                        foodModel[Index].price.toString(),
                        style: const TextStyle(color: Colors.red),
                      )),
                ],
              ),
              InkWell(
                  onTap: () {
                    BlocProvider.of<OrderBloc>(context).add(OrderFoodEvent(
                        foodModel[Index].id.toString(),
                        foodModel[Index].name.toString(),
                        foodModel[Index].image.toString(),
                        foodModel[Index].price.toString(),
                        foodModel[Index].description.toString(),
                        "1"));
                    ;
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text("Thành công"),
                      duration: Duration(milliseconds: 500),
                    ));
                  },
                  child: Container(
                    alignment: Alignment.topRight,
                    child: const Icon(
                      Icons.shopping_cart,
                      color: Colors.blue,
                    ),
                  ))
            ],
          ),
        ],
      ),
    ),
  );
}
