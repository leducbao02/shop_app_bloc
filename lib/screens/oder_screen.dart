import 'package:bloc_http_flutter/bloc/bloc_food/bloc_food_bloc.dart';
import 'package:bloc_http_flutter/bloc/bloc_order/order_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'details_screen.dart';

class OderScreen extends StatefulWidget {
  const OderScreen({Key? key}) : super(key: key);

  @override
  State<OderScreen> createState() => _OderScreenState();
}

class _OderScreenState extends State<OderScreen> {
  double totalPrice = 0;
  int totalQuantity = 0;
  int Index = -1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Column(
        children: [
          Container(
            color: Colors.blueAccent,
            height: 100,
            child: const Center(
                child: Text(
              'ORDER',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            )),
          ),
          BlocBuilder<OrderBloc, OrderState>(
            builder: (context, state) {
              if (state is OderError) {
                return Text(state.mess);
              }
              if (state is OrderSuccess) {
                return InkWell(
                  onTap: () {
                    state.listOrder.forEach((element) {
                      Index++;
                    });
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (ctx) =>
                                DetailsScreen(
                                  item: state.listOrder,
                                  index: Index,
                                )));
                  },
                  child: Expanded(
                    child: ListView.builder(
                        itemCount: state.listOrder.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) => Card(
                              child: ListTile(
                                title: Wrap(
                                  children: [
                                    Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.all(10),
                                          child: SizedBox(
                                            height: 90,
                                            width: 63,
                                            child: Image.network(state
                                                .listOrder[index].image
                                                .toString()),
                                          ),
                                        ),
                                        Column(
                                          children: [
                                            SizedBox(
                                              width: 200,
                                              child: Text(
                                                overflow: TextOverflow.ellipsis,
                                                state.listOrder[index].name
                                                    .toString(),
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20),
                                              ),
                                            ),
                                            Container(
                                                alignment: Alignment.topLeft,
                                                margin: const EdgeInsets.fromLTRB(
                                                    0, 10, 140, 0),
                                                child: Text(
                                                  state.listOrder[index].price
                                                      .toString(),
                                                  style: const TextStyle(
                                                      color: Colors.red),
                                                )),
                                            Container(
                                                alignment: Alignment.topLeft,
                                                margin: const EdgeInsets.fromLTRB(
                                                    0, 0, 150, 0),
                                                child: Text(state
                                                    .listOrder[index].quantity
                                                    .toString())),
                                          ],
                                        ),
                                        InkWell(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (ctx) =>
                                                          DetailsScreen(
                                                            item: state.listOrder,
                                                            index: index,
                                                          )));
                                              print("object");
                                            },
                                            child: const Icon(
                                                Icons.keyboard_arrow_right))
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            )),
                  ),
                );

                // print(state.listOrder.length);
              }
              return Container();
            },
          ),
          BlocBuilder<OrderBloc, OrderState>(builder: (context, state) {
            print(state);
            if (state is OrderSuccess) {
              for (var element in state.listOrder) {
                totalQuantity += int.parse(element.quantity ?? "0");
                totalPrice +=
                    double.parse(element.price ?? "0") * totalQuantity;
              }
              return Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: const Text(
                            "Total",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          )),
                      Container(
                          margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                          child: Text(totalQuantity.toString())),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: const Text(
                            "Quantity",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          )),
                      Container(
                          margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                          child: Text(totalPrice.toString())),
                    ],
                  )
                ],
              );
            }

            return Container();
          }),
        ],
      ),
    );
  }
}
