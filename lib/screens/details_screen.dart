import 'package:bloc_http_flutter/models/food_model_order.dart';
import 'package:flutter/material.dart';

class DetailsScreen extends StatefulWidget {
  const DetailsScreen({Key? key, required this.item, required this.index})
      : super(key: key);
  final List<FoodModelOrder> item;

  final int index;

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  double price = 0;

  @override
  void initState() {
    // TODO: implement initState
    price += double.parse((widget.item[widget.index].price).toString()) * int.parse((widget.item[widget.index].quantity).toString());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.blueAccent,
              height: 100,
              child: Center(
                  child: Row(
                children: [
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 105, 0),
                    child: InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: const Icon(
                          Icons.keyboard_arrow_left,
                          color: Colors.white,
                        )),
                  ),
                  Center(
                      child: Text(
                    widget.item[widget.index].name.toString(),
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ))
                ],
              )),
            ),

            Padding(
              padding: const EdgeInsets.all(10),
              child: Image.network(widget.item[widget.index].image.toString()),
            ),

            Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                child: Text(widget.item[widget.index].description.toString())),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: const Text(
                      "Price",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Text(widget.item[widget.index].price.toString()))
              ],
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: const Text("Total",
                        style: TextStyle(fontWeight: FontWeight.bold))),
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Text(price.toString()))
              ],
            ),
            //
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: const Text("Quantity",
                        style: TextStyle(fontWeight: FontWeight.bold))),
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Text(widget.item[widget.index].quantity.toString()))
              ],
            ),


          ],
        ),
      ),
    );
  }
}
