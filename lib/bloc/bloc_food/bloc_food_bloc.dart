import 'package:bloc/bloc.dart';
import 'package:bloc_http_flutter/services/api_food_sevice.dart';
import 'package:bloc_http_flutter/services/api_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import '../../models/food_model_order.dart';
import 'bloc_food_event.dart';
import 'bloc_food_state.dart';

class FoodBloc extends Bloc<FoodEvent, FoodState> {
  final ApiRepository _apiRepository = ApiRepository();
  final FoodApi _foodApi = FoodApi();
  final List<FoodModelOrder> foodOrder = [];

  FoodBloc() : super(FoodInitial()) {
    on<FetchLisFood>((event, emit) async {
      try {
        emit(FoodLoading());
        final list = await _apiRepository.fetchListFood();
        emit(FoodLoaded(list));
      } catch (e) {
        emit(const FoodLoadError("Error"));
      }
      // TODO: implement event handler
    });

    on<AddFoodOrder>((event, emit) {
      try {
        FoodModelOrder order = FoodModelOrder(
            id: event.id,
            name: event.name,
            image: event.image,
            price: event.price,
            description: event.description,
            quantity: "1");
        foodOrder.add(order);
        emit(AddOdrerSuccess(foodOrder));
      } catch (e) {
        emit(const AddFoodOrderError("error"));
      }
    });

    on<AddFood>((event, emit) async {
      emit(FoodLoading());
      http.Response res = await _foodApi.addFood(
          event.name, event.image, event.price, event.description);
      if (res.statusCode == 201) {
        final list = await _apiRepository.fetchListFood();
        emit(FoodLoaded(list));
        _back(event);
      } else {
        emit(const FoodLoadError("AddError"));
      }
    });

    on((event, emit) async {});
  }

  void _back(event) {
    Navigator.pop(event.context);
  }
}
