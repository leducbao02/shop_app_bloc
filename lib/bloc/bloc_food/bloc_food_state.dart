import 'package:equatable/equatable.dart';

import '../../models/Food_model.dart';
import '../../models/food_model_order.dart';



abstract class FoodState extends Equatable {
  const FoodState();
}

class FoodInitial extends FoodState {
  @override
  List<Object> get props => [];
}

class FoodLoading extends FoodState{
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();

}

class FoodLoaded extends FoodState{
  final List<FoodModel> listFood;
  const FoodLoaded(this.listFood);
  @override
  // TODO: implement props
  List<Object?> get props => [listFood];
}



class FoodLoadError extends FoodState{
  final String messge;
  const FoodLoadError(this.messge);
  @override
  // TODO: implement props
  List<Object?> get props => [messge];

}



class AddOdrerSuccess extends FoodState{
  final List<FoodModelOrder> listOrder;
  const AddOdrerSuccess(this.listOrder);
  @override
  // TODO: implement props
  List<Object?> get props => [listOrder];
}

class AddFoodOrderError extends FoodState{
  final String error;
  const AddFoodOrderError(this.error);
  @override
  // TODO: implement props
  List<Object?> get props => [error];

}

