part of 'order_bloc.dart';

abstract class OrderState extends Equatable {
  const OrderState();
}

class OrderInitial extends OrderState {
  @override
  List<Object> get props => [];
}

class OrderSuccess extends OrderState{
  List<FoodModelOrder> listOrder;
  OrderSuccess(this.listOrder);
  @override
  // TODO: implement props
  List<Object?> get props => [listOrder];

}




class OderError extends OrderState{
  String mess;
  OderError(this.mess);
  @override
  // TODO: implement props
  List<Object?> get props => [mess];

}

