import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:bloc_http_flutter/bloc/bloc_food/bloc_food_bloc.dart';
import 'package:bloc_http_flutter/models/food_model_order.dart';
import 'package:bloc_http_flutter/screens/oder_screen.dart';
import 'package:equatable/equatable.dart';

import '../../services/api_repository.dart';

part 'order_event.dart';

part 'order_state.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  List<FoodModelOrder> listOrder = [];

  // FoodModelOrder oder = FoodModelOrder();
  FoodModelOrder oderNew = FoodModelOrder();

  OrderBloc() : super(OrderInitial()) {
    on<OrderEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<OrderFoodEvent>((event, emit) {
      int index = -1;
      try {
        bool check = false;
        for (var element in listOrder) {
          index++;
          if (element.id == event.id) {
            int quan =
                int.parse(element.quantity ?? "0") + int.parse(event.quantity);
            listOrder[index] = element.copyWith(quantity: quan.toString());
            check = true;
          }
        }

        if (check) {
        } else {
          FoodModelOrder oder = FoodModelOrder(
              id: event.id,
              name: event.name,
              image: event.image,
              price: event.price,
              description: event.descrpiton,
              quantity: event.quantity.toString());
          listOrder.add(oder);
        }

        emit(OrderSuccess(listOrder));
      } catch (e) {
        emit(OderError("error"));
      }
    });
  }
}
