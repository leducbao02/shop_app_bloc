import 'dart:convert';

import 'package:bloc_http_flutter/models/Food_model.dart';
import 'package:http/http.dart' as http;

class FoodApi {
  var url = Uri.parse('https://63eafe34f1a969340db02811.mockapi.io/food');

  Future<List<FoodModel>> fetchListFood() async {
    var respone = await http.get(url);
    if (respone.statusCode == 200) {
      final parsed = jsonDecode(utf8.decode(respone.bodyBytes)).cast<Map<String, dynamic>>();
      return parsed.map<FoodModel>((json) => FoodModel.fromJson(json)).toList();
    } else {
      throw Exception("error");
    }
  }

  Future<http.Response> addFood(
      String name, String image, String price, String des) async {
    final bodyData = {
      "name": name,
      "image": image,
      "price": price,
      "description": des
    };
    final http.Response response = await http.post(url,
        body: jsonEncode(bodyData),
        headers: {"Content-Type": "application/json"});
    print(response.body.toString());
    return response;
  }
}
