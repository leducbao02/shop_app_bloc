class FoodModel {
  FoodModel({
     this.createdAt,
     this.name,
     this.image,
     this.price,
     this.description,
     this.id,
  });

  FoodModel.fromJson(dynamic json) {
    createdAt = json['createdAt'];
    name = json['name'];
    image = json['image'];
    price = json['price'];
    description = json['description'];
    id = json['id'];
  }

   String? createdAt;
   String? name;
  String?  image;
  String?  price;
  String?  description;
  String?  id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['createdAt'] = createdAt;
    map['name'] = name;
    map['image'] = image;
    map['price'] = price;
    map['description'] = description;
    map['id'] = id;
    return map;
  }
}
