class FoodModelOrder {
  FoodModelOrder({
    String? createdAt,
    String? name,
    String? image,
    String? price,
    String? description,
    String? id,
    String? quantity,}){
    _createdAt = createdAt;
    _name = name;
    _image = image;
    _price = price;
    _description = description;
    _id = id;
    _quantity = quantity;
  }

  FoodModelOrder.fromJson(dynamic json) {
    _createdAt = json['createdAt'];
    _name = json['name'];
    _image = json['image'];
    _price = json['price'];
    _description = json['description'];
    _id = json['id'];
    _quantity = json['quantity'];
  }
  String? _createdAt;
  String? _name;
  String? _image;
  String? _price;
  String? _description;
  String? _id;
  String? _quantity;
  FoodModelOrder copyWith({  String? createdAt,
    String? name,
    String? image,
    String? price,
    String? description,
    String? id,
    String? quantity,
  }) => FoodModelOrder(  createdAt: createdAt ?? _createdAt,
    name: name ?? _name,
    image: image ?? _image,
    price: price ?? _price,
    description: description ?? _description,
    id: id ?? _id,
    quantity: quantity ?? _quantity,
  );
  String? get createdAt => _createdAt;
  String? get name => _name;
  String? get image => _image;
  String? get price => _price;
  String? get description => _description;
  String? get id => _id;
  String? get quantity => _quantity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['createdAt'] = _createdAt;
    map['name'] = _name;
    map['image'] = _image;
    map['price'] = _price;
    map['description'] = _description;
    map['id'] = _id;
    map['quantity'] = _quantity;
    return map;
  }

}