import 'package:bloc_http_flutter/bloc/bloc_food/bloc_food_bloc.dart';
import 'package:bloc_http_flutter/bloc/bloc_order/order_bloc.dart';
import 'package:bloc_http_flutter/screens/home_screen.dart';
import 'package:bloc_http_flutter/screens/navigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => FoodBloc()),
        BlocProvider(
          create: (context) => OrderBloc(),
        ),
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: ScreenNav(),
      )));
}
